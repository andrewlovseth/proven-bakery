	<footer>
		<div class="wrapper">

			<div class="footer-wrapper">
				<?php the_field('footer_message', 'options'); ?>
			</div>

		</div>
	</footer>

	<?php get_template_part('partials/footer/scripts'); ?>

	<?php wp_footer(); ?>

</body>
</html>