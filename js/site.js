$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('nav.mobile').slideToggle(300);
		return false;
	});


	$('nav .main-nav a').smoothScroll();

});
