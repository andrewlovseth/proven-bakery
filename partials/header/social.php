<div class="social">
	<a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external">
		<img src="/wp-content/themes/proven-bakery/images/facebook.svg" alt="Facebook" />
	</a>
	
	<a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external">
		<img src="/wp-content/themes/proven-bakery/images/twitter.svg" alt="Twitter" />
	</a>
	
	<a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external">
		<img src="/wp-content/themes/proven-bakery/images/instagram.svg" alt="Instagram" />
	</a>

	<a href="http://www.heavyrestaurantgroup.com/news-events/" rel="external" class="blog">
		<img src="/wp-content/themes/proven-bakery/images/rss.svg" alt="Blog" />
	</a>
</div>