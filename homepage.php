<?php

/*

	Template Name: Home

*/

get_header(); ?>

<?php if(get_field('show_announcement')): ?>

	<section id="announcement">
		<div class="wrapper">

			<div class="announcement-wrapper">
				<?php the_field('announcement'); ?>
			</div>

		</div>
	</section>

<?php endif; ?>


<section id="hero">
	<div class="wrapper">

		<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

		<div class="about-statement">
			<?php the_field('about_statement'); ?>
		</div>

	</div>
</section>


<section id="wholesale">
	<div class="wrapper">

		<div class="wholesale-wrapper">

			<div class="info">
				<h2 class="section-header">Wholesale</h2>
				<?php the_field('wholesale_info'); ?>
			</div>

			<div class="photo">
				<img src="<?php $image = get_field('wholesale_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>

	</div>
</section>


<section id="gallery">
	<div class="wrapper">

		<div class="gallery-wrapper">
			<?php $photoIDs = ''; $images = get_field('gallery'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>
					
					<?php $photoIDs .= $image['ID'] . ','; ?>

				<?php endforeach; ?>
			<?php endif; ?>

			<?php echo do_shortcode('[gallery ids="' . $photoIDs . '" size="medium"]'); ?>
		</div>

	</div>
</section>


<section id="menu">
	<div class="wrapper">

		<div class="menu-wrapper">
			<h2 class="section-header">Menu</h2>

			<?php if(have_rows('menus')): while(have_rows('menus')): the_row(); ?>
			 
			    <div class="menu-section">
					<div class="mobile menu">
						<img src="<?php $image = get_sub_field('mobile'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="desktop menu">
						<img src="<?php $image = get_sub_field('desktop'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
			    </div>

			<?php endwhile; endif; ?>

		</div>

	</div>
</section>


<section id="find-us">
	<div class="wrapper">

		<div class="find-us-wrapper">

			<div class="info">
				<h2 class="section-header">Find Us</h2>
				<?php the_field('find_us_info'); ?>
			</div>

			<div class="list">
				<?php if(have_rows('find_us_list')): while(have_rows('find_us_list')): the_row(); ?>
					<p><?php the_sub_field('name'); ?></p>
				<?php endwhile; endif; ?>
			</div>

		</div>

	</div>
</section>


<?php get_footer(); ?>